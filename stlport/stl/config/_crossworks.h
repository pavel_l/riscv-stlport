#ifndef __CROSSWORKS_CONFIG
#define __CROSSWORKS_CONFIG

#define _STLP_LONG_LONG long long
#define _STLP_NO_LONG_DOUBLE 1
#ifndef __EXCEPTIONS
#define _STLP_DONT_USE_EXCEPTIONS 1
#define _STLP_NO_BAD_ALLOC  1
#else
#define _STLP_THROW_RANGE_ERRORS
#endif

#if defined(__BIG_ENDIAN)
#define _STLP_BIG_ENDIAN 1
#else
#define _STLP_LITTLE_ENDIAN 1
#endif

#ifndef __CROSSWORKS_ARM_USE_IOSTREAMS
#define _STLP_NO_IOSTREAMS 1
#else
#define _STLP_USE_STDIO_IO
#define _STLP_UNIX
#endif

#define _NOTHREADS

#define _STLP_COMPILER 1

#ifdef __SHORT_WCHAR
#define _STLP_WCHAR_T_IS_USHORT
#endif

#endif
